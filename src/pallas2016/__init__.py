"""
Data and formalisms from Pallas et al. (2016)
"""
# {# pkglts, src
# FYEO
# #}
# {# pkglts, version, after src
from . import version

__version__ = version.__version__
# #}
# {# pkglts, glabdata, after version
from .info import *
# #}
