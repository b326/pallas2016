"""
Format tree data from fig3
"""
from pathlib import Path

import pandas as pd
from graphextract.svg_extractor import extract_data

from pallas2016 import pth_clean

dfs = []
for suffix in ("a", "b", "c", "d"):
    data = extract_data(f"../raw/fig3{suffix}.svg", x_formatter=str, y_formatter=int, x_type='bin')
    (vname, pts), = data.items()
    df = pd.DataFrame(
        {
            'tree': [f'fuji_{lab}' for lab, _ in pts],
            vname: [val for _, val in pts]
        }
    ).set_index(['tree'])
    dfs.append(df)

df = pd.concat(dfs, axis='columns')

# write resulting dataframe in a csv file
df = df[sorted(df.columns)]

with open(pth_clean / "fig3.csv", 'w', encoding='utf-8') as fhw:
    fhw.write(f"# This file has been generated by {Path(__file__).name}\n#\n")
    fhw.write("#\n")
    fhw.write("# tree: [-] name of observed tree\n")
    fhw.write("# dm_harvest: [kg DM] total fruit dry weight\n")
    fhw.write("# leaf_area: [m2] tree leaf area\n")
    fhw.write("# nb_fruit: [#]\n")
    fhw.write("# nb_shoot: [#]\n")
    fhw.write("#\n")
    df.to_csv(fhw, sep=";", lineterminator="\n", float_format="%.2f")
