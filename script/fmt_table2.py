"""
Format data in table2
"""
from pathlib import Path

import pandas as pd

from pallas2016 import pth_clean

df = pd.read_csv("../raw/table2.csv", sep=";", comment="#")

# write resulting dataframe
df = df.set_index(['tree'])
df = df[sorted(df.columns)]
df = df.sort_index()

with open(pth_clean / "table2.csv", 'w', encoding="utf-8") as fhw:
    fhw.write(f"# This file has been generated by {Path(__file__).name}\n#\n")
    fhw.write("# tree: [-] name of observed tree\n")
    fhw.write("# crop_load: [# fruit.m-2]\n")
    fhw.write("# dm_fruit: [g DM] mean dry mass of single fruit\n")
    fhw.write("# dm_fruit_sd: [g DM] standard deviation dry mass of single fruit\n")
    fhw.write("#\n")
    df.to_csv(fhw, sep=";", lineterminator="\n", float_format="%.2f")
