========================
pallas2016
========================

.. {# pkglts, doc

.. image:: https://b326.gitlab.io/pallas2016/_images/badge_pkging_pip.svg
    :alt: PyPI version
    :target: https://pypi.org/project/pallas2016/1.1.0/

.. image:: https://b326.gitlab.io/pallas2016/_images/badge_pkging_conda.svg
    :alt: Conda version
    :target: https://anaconda.org/revesansparole/pallas2016

.. image:: https://b326.gitlab.io/pallas2016/_images/badge_doc.svg
    :alt: Documentation status
    :target: https://b326.gitlab.io/pallas2016/

.. image:: https://badge.fury.io/py/pallas2016.svg
    :alt: PyPI version
    :target: https://badge.fury.io/py/pallas2016

.. #}
.. {# pkglts, glabpkg_dev, after doc

main: |main_build|_ |main_coverage|_

.. |main_build| image:: https://gitlab.com/b326/pallas2016/badges/main/pipeline.svg
.. _main_build: https://gitlab.com/b326/pallas2016/commits/main

.. |main_coverage| image:: https://gitlab.com/b326/pallas2016/badges/main/coverage.svg
.. _main_coverage: https://gitlab.com/b326/pallas2016/commits/main
.. #}

Data and formalisms from Pallas et al. (2016)

