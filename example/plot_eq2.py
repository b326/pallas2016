"""
Equation 2
==========

Photosynthesis light response curve
"""

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from pallas2016 import pth_clean, raw

# read data
params = pd.read_csv(pth_clean / "table_s1.csv", sep=";", comment="#", index_col=['name'])['value'].to_dict()
temp = 20  # [°C] leaf temperature

dm = params['sb_ls_medium']
dm_leaf = dm * params['r_l']

p_max = params['p1']
l_i = dm_leaf / params['slw']
m_resp_coeff = params[f'mrr_l'] * params[f'q10_l'] ** ((temp - 20) / 10)
rd = m_resp_coeff * params['slw'] / 12e-6  # [g C.g DM-1.s-1] to [µmol CO2.m-2.s-1]

# compute curves
records = []
for ppfd in np.linspace(0, 2000, 100):
    records.append(dict(
        ppfd=ppfd,
        ag=raw.eq2(ppfd, p_max, rd, l_i, params['p4'])
    ))

df = pd.DataFrame(records).set_index(['ppfd'])

ag_max = (p_max + rd) * l_i * 12e-6 * 3600
m_resp = rd * l_i * 12e-6 * 3600
# plot result
fig, axes = plt.subplots(1, 1, sharex='all', figsize=(9, 6), squeeze=False)
ax = axes[0, 0]
ax.plot(df.index, df['ag'])
ax.axhline(y=0, ls='--', color='#444444')
ax.axhline(y=ag_max, ls='--', color='#aaaaaa')
ax.text(0, ag_max, "ag max", ha='left', va='top')
ax.axhline(y=m_resp, ls='--', color='#aaaaaa')
ax.text(0, m_resp, "m_resp", ha='left', va='bottom')

ax.set_ylabel("Ag [g C.h-1.shoot-1]")
ax.set_xlim(0, 2000)
ax.set_xlabel("ppfd [µmol photon.s-1]")

fig.tight_layout()
plt.show()
