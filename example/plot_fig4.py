"""
Figure 4
========

Plot measures from figure 4
"""
import matplotlib.pyplot as plt
import pandas as pd

from pallas2016 import pth_clean

# read meas
meas = pd.read_csv(pth_clean / "fig4.csv", sep=";", comment="#", index_col=['doy'])

# plot result
fig, axes = plt.subplots(3, 1, sharex='all', figsize=(6, 8), squeeze=False)
ax = axes[0, 0]
for cpt in ['root', 'wood', 'leafy_shoot', 'fruit']:
    ax.plot(meas.index, meas[f'{cpt}_dm'], '-', label=cpt)

ax.legend(loc='upper left')
ax.set_ylabel("biomass [g DM]")
ax.set_ylim(0, 3200)
ax.set_yticks(range(0, 3200, 1000))

ax = axes[1, 0]
for cpt in ['root', 'wood', 'leafy_shoot', 'fruit']:
    ax.plot(meas.index, meas[f'{cpt}_growth_demand'], '-', label=cpt)

ax.set_ylabel("growth demand [g DM.day-1]")
ax.set_ylim(0, 50)
ax.set_yticks(range(0, 50, 20))

ax = axes[2, 0]
ax.plot(meas.index, meas['photo'], '-')

ax.set_ylabel("tree photo [g C.day-1]")
ax.set_ylim(0, 79)
ax.set_yticks(range(0, 79, 20))

ax.set_xlabel("doy")
# ax.set_xlim(110, 250)
# ax.set_xticks(range(110, 251, 20))

fig.tight_layout()
plt.show()
