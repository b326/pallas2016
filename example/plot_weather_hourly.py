"""
Typical hourly weather
======================

Hourly weather from NASA
"""

import matplotlib.pyplot as plt
import pandas as pd

from pallas2016 import pth_clean

# read data
df = pd.read_csv(pth_clean / "weather_hourly.csv", sep=";", comment="#", parse_dates=['date'], index_col=['date'])

# plot
fig, axes = plt.subplots(4, 1, sharex='all', figsize=(12, 6), squeeze=False)

ax = axes[0, 0]
ax.plot(df.index, df['rg'], label='rg')
ax.plot(df.index, df['par'], label='par')
ax.legend(loc='upper left')
ax.set_ylabel("[W.m-2]")

ax = axes[1, 0]
ax.plot(df.index, df['temp'], label="temp")
ax.legend(loc='upper left')
ax.set_ylabel("[°C]")

ax = axes[2, 0]
ax.plot(df.index, df['rh'], label="rh")
ax.legend(loc='upper left')
ax.set_ylabel("[-]")

ax = axes[3, 0]
ax.plot(df.index, df['ws'], label="wind speed")
ax.legend(loc='upper left')
ax.set_ylabel("[m.s-1]")

ax.set_xlabel("UTC")

fig.tight_layout()
plt.show()
