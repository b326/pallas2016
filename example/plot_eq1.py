"""
Equation 1
==========

Evolution of Pmax with status of reserves in leaves

.. warning::

    Apparently the formalism is wrong since photosynthesis increases with increasing
    reserves which is opposite of what's expected and written in the text.

"""

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd

from pallas2016 import pth_clean, raw

# read data
params = pd.read_csv(pth_clean / "table_s1.csv", sep=";", comment="#", index_col=['name'])['value'].to_dict()

ltb = params['sb_ls_medium'] * params['r_l']

# compute curves
records = []
for lrb in np.linspace(0, ltb, 100):
    records.append(dict(
        lrb=lrb,
        ltb=ltb,
        p_max=raw.eq1(lrb, ltb, params['p1'], params['p2'])
    ))

df = pd.DataFrame(records).set_index(['lrb'])

# plot result
fig, axes = plt.subplots(1, 1, sharex='all', figsize=(9, 6), squeeze=False)
ax = axes[0, 0]
ax.plot(df.index, df['p_max'])
ax.axvline(x=ltb, ls='--', color='#aaaaaa')
ax.text(ltb, 50, "LTB", ha='right', va='center', rotation=90)

ax.set_ylabel("p_max [µmol CO2.m-2.s-1]")
ax.set_xlabel("RTB [g DM.shoot-1]")
fig.tight_layout()
plt.show()
