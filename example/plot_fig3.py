"""
Figure 3
========

Plot measures from figure 3
"""
import matplotlib.pyplot as plt
import pandas as pd

from pallas2016 import pth_clean

# read meas
meas = pd.read_csv(pth_clean / "fig3.csv", sep=";", comment="#", index_col=['tree'])

# empty space between measured and simulated trees
meas = meas.reindex(meas.index.insert(2, ''))

# plot result
fig, axes = plt.subplots(2, 2, sharex='all', figsize=(8, 8), squeeze=False)
ax = axes[0, 0]
ax.bar(meas.index, meas['nb_shoot'])

ax.set_ylabel("Shoot number")
ax.set_ylim(0, 1800)
ax.set_yticks(range(0, 1800, 500))

ax = axes[0, 1]
ax.bar(meas.index, meas['leaf_area'])

ax.set_ylabel("Leaf area [m2]")
ax.set_ylim(0, 32)
ax.set_yticks(range(0, 32, 5))

ax = axes[1, 0]
ax.bar(meas.index, meas['nb_fruit'])

ax.set_ylabel("fruit number")
ax.set_ylim(0, 150)
ax.set_yticks(range(0, 140, 50))

ax = axes[1, 1]
ax.bar(meas.index, meas['dm_harvest'])

ax.set_ylabel("Total fruit dry weight [kg]")
ax.set_ylim(0, 5)
ax.set_yticks(range(0, 5, 1))

for ax in axes[-1, :]:
    ax.tick_params(axis='x', labelrotation=45)

fig.tight_layout()
plt.show()
