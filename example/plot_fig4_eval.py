"""
Figure 4 evaluation
===================

Evaluation around fig4 data.
"""
import matplotlib.pyplot as plt
import pandas as pd

from pallas2016 import pth_clean

# read meas
params = pd.read_csv(pth_clean / "table_s1.csv", sep=";", comment="#", index_col=['name'])['value'].to_dict()

meas = pd.read_csv(pth_clean / "fig4.csv", sep=";", comment="#", index_col=['doy'])

dm_ini = {cpt: meas[f'{cpt}_dm'].iloc[0] for cpt in ['root', 'wood', 'leafy_shoot', 'fruit']}

m_resp = dict(root=params['mrr_or'],  # [g C.g DM-1.s-1]
              wood=params['mrr_ow'],
              leafy_shoot=params['mrr_l'] * params['r_l'] + params['mrr_nl'] * (1 - params['r_l']),
              fruit=params['mrr_f'])
g_resp = dict(root=params['grc_or'],  # [g C.g DM-1]
              wood=params['grc_ow'],
              leafy_shoot=params['grc_l'] * params['r_l'] + params['grc_nl'] * (1 - params['r_l']),
              fruit=params['grc_f'])
cc = dict(root=params['cc_or'], wood=params['cc_ow'], leafy_shoot=params['cc_l'], fruit=params['cc_f'])  # [g C.g DM-1]

for cpt in ['root', 'wood', 'leafy_shoot', 'fruit']:
    meas[f'{cpt}_growth'] = meas[f'{cpt}_dm'].diff().shift(-1).fillna(method='ffill')
    meas[f'{cpt}_g_resp'] = meas[f'{cpt}_growth'] * g_resp[cpt]

    meas[f'{cpt}_dc'] = meas[f'{cpt}_growth'] * cc[cpt] + meas[f'{cpt}_g_resp']
    meas[f'{cpt}_m_resp'] = meas[f'{cpt}_dm'] * m_resp[cpt] * 24 * 3600  # [g C.day-1]

dc_tot = meas[[f'{cpt}_dc' for cpt in ['root', 'wood', 'leafy_shoot', 'fruit']]].sum(axis='columns')
mr_tot = meas[[f'{cpt}_m_resp' for cpt in ['root', 'wood', 'leafy_shoot', 'fruit']]].sum(axis='columns')
storage = meas['photo'] - dc_tot - mr_tot

sto_use_max = -storage.cumsum().min()

# plot result
fig, axes = plt.subplots(3, 1, sharex='all', figsize=(10, 9), squeeze=False)
ax = axes[0, 0]
for cpt in ['root', 'wood', 'leafy_shoot', 'fruit']:
    crv, = ax.plot(meas.index, meas[f'{cpt}_dm'], '-', label=cpt)
    ax.plot(meas.index, meas[f'{cpt}_growth'].cumsum() + dm_ini[cpt], '--', color=crv.get_color())

ax.legend(loc='upper left')
ax.set_ylabel("biomass [g DM]")
ax.set_ylim(0, 3200)
ax.set_yticks(range(0, 3200, 1000))

ax = axes[1, 0]
for cpt in ['root', 'wood', 'leafy_shoot', 'fruit']:
    crv, = ax.plot(meas.index, meas[f'{cpt}_growth_demand'], '-', label=cpt)
    ax.plot(meas.index, meas[f'{cpt}_growth'] + meas[f'{cpt}_g_resp'] * 30 / 12, '--', color=crv.get_color())

ax.set_ylabel("growth [g DM.day-1]")
ax.set_ylim(0, 50)
ax.set_yticks(range(0, 50, 20))

ax = axes[2, 0]
ax.plot(meas.index, meas['photo'], '-', label="photo")
ax.plot(mr_tot.index, mr_tot, '--', label="m_resp")
ax.plot(dc_tot.index, mr_tot + dc_tot, '--', label="growth + m_resp")
ax.legend(loc='upper left')

ax.set_ylabel("tree photo [g C.day-1]")
ax.set_ylim(0, 79)
ax.set_yticks(range(0, 79, 20))

ax.set_xlabel("doy")

fig.tight_layout()

# storage evolution
fig, axes = plt.subplots(1, 1, figsize=(8, 5), squeeze=False)
ax = axes[0, 0]
ax.plot(storage.index, storage.cumsum())
ax.axhline(y=0, ls='--', color='#aaaaaa')
ax.text(0.1, 0.9, f"max storage use {sto_use_max:.2f} [g C], "
                  f"{sto_use_max / 12:.2f} [mol C], "
                  f"{sto_use_max / 12 * 30 / dm_ini['wood'] * 100:.1f} [%] of wood",
        ha='left', va='top', transform=ax.transAxes)
ax.set_ylabel("storage evolution [g C]")
ax.set_xlabel("doy")

fig.tight_layout()

plt.show()
