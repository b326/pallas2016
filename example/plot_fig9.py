"""
Figure 9
========

Plot measures from figure 9
"""
import matplotlib.pyplot as plt
import pandas as pd

from pallas2016 import pth_clean

# read meas
meas = pd.read_csv(pth_clean / "fig9.csv", sep=";", comment="#", index_col=['crop_load'])

# plot result
fig, axes = plt.subplots(3, 1, sharex='all', figsize=(6, 8), squeeze=False)
ax = axes[0, 0]
ax.plot(meas.index, meas['fruit_weight'], '-o', label="mean")

ax.legend(loc='upper left')
ax.set_ylabel("fruit weight [g DM]")
ax.set_ylim(0, 80)
ax.set_yticks(range(0, 61, 20))

ax = axes[1, 0]
ax.plot(meas.index, meas['leaf_area'], '-o', label="leaf area")

ax.legend(loc='upper left')
ax.set_ylabel("leaf area [m2]")
ax.set_ylim(12, 20)
ax.set_yticks(range(12, 19, 2))

axt = ax.twinx()
axt.plot(meas.index, meas['reserve'], '-s', label="reserve")

axt.legend(loc='upper right')
axt.set_ylabel("reserve [g.g-1]")
axt.set_ylim(0, 0.04)
axt.set_yticks([0, 0.01, 0.02, 0.03])

ax = axes[2, 0]
ax.plot(meas.index, meas['par_intercept'], '-o', label="iPAR")

ax.legend(loc='upper left')
ax.set_ylabel("PAR/fruit [mol photon]")
ax.set_ylim(0, 2200)
ax.set_yticks(range(0, 2200, 500))

axt = ax.twinx()
axt.plot(meas.index, meas['pn_eff'], '-s', label="Pn efficiency")

axt.legend(loc='upper right')
axt.set_ylabel("Pn eff [g.mol-1]")
axt.set_ylim(0, 0.25)
axt.set_yticks([0, 0.05, 0.1, 0.15, 0.2])

ax.set_xlabel("crop load [# fruit.m-2]")
ax.set_xlim(0, 16)
ax.set_xticks(range(0, 16, 5))

fig.tight_layout()
plt.show()
